import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

//import { Menu } from '../models';

@Injectable({ providedIn: 'root' })

export class MenuService {

    constructor(private http: HttpClient) { }
    //${config.apiUrl}

    getMenu() {
        return this.http.get(`http://api.teo.com.co/api/menu`);
    }
}
