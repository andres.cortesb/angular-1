import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { PageModule } from './page/page.module';
import { AdminLayoutComponent } from './@theme/layouts/admin-layout/admin-layout.component'; 
import { ComponentsModule } from './@theme/components/components.module';

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    PageModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ComponentsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {}
