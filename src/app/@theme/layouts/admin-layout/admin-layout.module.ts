import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminLayoutRoutingModule } from './admin-layout-routing.module';

import { PageModule } from '../../../page/page.module';

@NgModule({
  imports: [
    CommonModule,
    AdminLayoutRoutingModule,
    PageModule,
  ],
  declarations: [
  ]
})

export class AdminLayoutModule {}
