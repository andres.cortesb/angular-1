import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CotizadorComponent } from "../../../page/cotizador/cotizador.component"

const routes: Routes = [
  {
    path: 'Cotizacion',
    component: CotizadorComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class AdminLayoutRoutingModule {}
