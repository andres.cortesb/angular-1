import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'comparaonline-cotizador',
  templateUrl: './cotizador.component.html',
  styleUrls: ['./cotizador.component.css'],
  encapsulation: ViewEncapsulation.None   
})

export class CotizadorComponent implements OnInit {

  CotizaForm  : FormGroup;
  submitted   : boolean = false;
  visible     : boolean = false;
  formg       : string  = 'form-group'; 
  formrow     : string  = 'row'; 

  constructor( private formBuilder: FormBuilder ) { }

  ngOnInit() {

    if (window.matchMedia(`(max-width: 567px)`).matches) {
      this.visible  = true;
      this.formg    = "";
      this.formrow  = 'row no-gutters';
    }else{
      this.visible  = false;
      this.formg    = 'form-group';
      this.formrow  = 'row';
    }

    this.CotizaForm = this.formBuilder.group({
      marca_auto  : ['', Validators.required],
      modelo_auto : ['', Validators.required],
      year        : ['', Validators.required],
      email       : ['', Validators.compose([Validators.required, Validators.email])]
    });

  }

  get f() { return this.CotizaForm.controls; }

  onSubmit(){

		this.submitted = true;

		if (this.CotizaForm.invalid) {
			return;
    }else{
      alert("Formulario Enviado Exitosamente");
    }
  
  }

}
